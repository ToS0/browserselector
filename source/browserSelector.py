#!/usr/bin/python2
# -*- coding: iso-8859-15 -*-

# Author and repo: https://gitlab.com/ToS0/browserselector

import os
import sys
import json
import unicodedata
from xdg import DesktopEntry
from xdg import IconTheme
from functools import partial
from PyQt4 import QtCore
from PyQt4.QtCore import *
from PyQt4 import QtGui
from PyQt4.QtGui import *

# needed for unicode support
reload(sys)
sys.setdefaultencoding('Cp1252')

# edit your paths and browsers here:
browsers = [
    "/home/**YOUR-USER**/.local/share/applications/chromium_chromium.desktop",
    "/home/**YOUR-USER**/.local/share/applications/brave_brave.desktop",
    "/home/**YOUR-USER**/.local/share/applications/epiphany_epiphany.desktop",
    "/home/**YOUR-USER**/.local/share/applications/firefox_firefox.desktop",
    "/home/**YOUR-USER**/.local/share/applications/vivaldi-stable.desktop",
]
config = "/home/**YOUR-USER**/**YOUR-PATH**/urls.json" # the file does need to have at least [] as content
iconSize = [48,48]
url = sys.argv[1]

class Selector(QtGui.QWidget):
    def __init__(self):
        super(Selector, self).__init__()
        self.buttons = []
        self.setupWindow()
        self.show()

    def setupWindow(self):
        cbox = QCheckBox()

        # create the buttons for the browsers
        counter = 0
        for browser in browsers:
            icon = QtGui.QIcon()
            # set the data from the desktop file
            icon.addPixmap(QtGui.QPixmap(IconTheme.getIconPath(DesktopEntry.DesktopEntry(browser).getIcon())))
            self.buttons += [QtGui.QPushButton("")]
            self.buttons[-1].setIcon(icon)
            self.buttons[-1].setIconSize(QtCore.QSize(iconSize[0],iconSize[1]))
            self.buttons[-1].setToolTip(DesktopEntry.DesktopEntry(browser).getComment())
            self.buttons[-1].clicked.connect(partial(self.buttonClicked, browser, cbox, counter))
            counter += 1

        # and the layout
        outerLayout = QtGui.QVBoxLayout()                
        hbox = QtGui.QHBoxLayout()
        form = QtGui.QFormLayout()

        # show URL after :// and before /
        global domain # is used for loading/saving either the domain or file name (basically URL without protocol and pathes)
        domain = url.split("/")
        if 'http:' in domain or 'https:' in domain:
            domain = domain[2]
            label = ('~' + domain[-20:]) if len(domain) > 20 else domain
        else:
            # if it is a file, show (shortend) name after last /
            domain = url.split("/") 
            domain = domain[len(domain)-1]
            label = domain.rsplit(".", 1)
            file = label[0]
            file = (file[:20] + '~') if len(file) > 20 else file
            ext = label[len(label)-1]
            label = unicode(file, "utf-8") + "." + ext
        cbox.setText("RMBR '" + label + "'?")
        form.addWidget(cbox)

        # check if needed browser is already specified
        global json_array
        input_file = open (config)
        json_array = json.load(input_file)
        for item in json_array:
            if domain == item['url'] or domain == "www." + item['url']:
                self.buttonClicked(browsers[item['browser']], cbox, counter)

        # the buttons
        for button in self.buttons:
            hbox.addWidget(button)
        
        # create the layout
        outerLayout.addLayout(hbox)
        outerLayout.addLayout(form)
        self.setLayout(outerLayout)

        # focus to 1st button to allow keyboard CR
        self.buttons[0].setDefault(True)
        self.buttons[0].setFocus()
        
        # move the window to the cursor
        pos = QtGui.QCursor().pos()
        self.move(pos.x()-(iconSize[0]+0), pos.y()-iconSize[1]-30)
        self.setWindowTitle("Open with")

    def buttonClicked(self, browser, cbox, counter):
        # make sure your .desktop file doesn't use a lower case %u...
        exec_name = DesktopEntry.DesktopEntry(browser).getExec().replace("%U", "'" + url + "'")
        # remember browser choice for the next time
        if cbox.isChecked():
            json_array.append({
                "url": domain,
                "browser": counter,
            })
            with open(config, 'w') as json_file:
                json.dump(json_array, json_file, indent=4, separators=(',',': '))
        # hit it, baby
        os.system(exec_name + " &")
        sys.exit()

def main():
    app = QtGui.QApplication(sys.argv)
    sel = Selector()
    app.exec_()

if __name__ == '__main__':
    main()
