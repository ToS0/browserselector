#!/bin/bash

# Setup PyQt4 repo
sudo add-apt-repository ppa:rock-core/qt4
sudo apt update

# Install python2
sudo apt install -y python2

# Install pyxdg Package
sudo apt install -y python-xdg

# Install additional packages
wget http://archive.ubuntu.com/ubuntu/pool/universe/q/qt-assistant-compat/libqtassistantclient4_4.6.3-7build1_amd64.deb
sudo apt-get install ./libqtassistantclient4_4.6.3-7build1_amd64.deb
wget http://archive.ubuntu.com/ubuntu/pool/universe/p/python-qt4/python-qt4_4.12.1+dfsg-2_amd64.deb
sudo apt-get install ./python-qt4_4.12.1+dfsg-2_amd64.deb
wget http://archive.ubuntu.com/ubuntu/pool/universe/p/python-pyaudio/python-pyaudio_0.2.11-1build2_amd64.deb
sudo apt-get install ./python-pyaudio_0.2.11-1build2_amd64.deb


